﻿using Snake.Enums;
using System;

namespace Snake
{
    public class Point
    {
        int _x;
        int _y;
        char _symbol;


        public char Symbol
        {
            get { return _symbol; }
            set { _symbol = value; }
        }


        public Point(Point snakeTail)
        {
            _x = snakeTail._x;
            _y = snakeTail._y;
            _symbol = snakeTail._symbol;
        }

        public Point(int x, int y, char symbol)
        {
            _x = x;
            _y = y;
            _symbol = symbol;

        }

        public void SetDirection(int i, DirectionEnum direction)
        {
            switch (direction)
            {
                case DirectionEnum.Left:
                    _x = _x - i;
                    break;
                case DirectionEnum.Right:
                    _x = _x + i;
                    break;
                case DirectionEnum.Up:
                    _y = _y - i;
                    break;
                case DirectionEnum.Down:
                    _y = _y + i;
                    break;

            }
        }

        public void ClearPoint()
        {
            _symbol = ' ';
            DrawPoint();
        }

        public void DrawPoint()
        {
            Console.SetCursorPosition(_x, _y);
            Console.Write(_symbol);
        }

        public bool ComparePoints(Point point)
        {
            return _x == point._x && _y == point._y;
        }
    }
}
