﻿using Snake.Enums;
using Snake.Factory;
using Snake.Helpers;
using Snake.Installers;
using Snake.UserServices;
using System;
using System.Threading;

namespace Snake
{
    public class GamePlay
    {
        UserService _userService = new UserService();
        public void StartGame(User user)
        {
            if (user == null)
                user = new User();

            int score = 0;

            LineInstaller line = new LineInstaller();
            line.DrawShapes();

            Point food = FoodFactory.GetRandomFood(49, 20, 'Х');
            Console.ForegroundColor = ColorHelper.GetRandomColor(new Random().Next(1, 5));
            food.DrawPoint();
            Console.ResetColor();

            Snake snake = new Snake();
            snake.CreateSnake(5, new Point(5, 5, 'Х'), DirectionEnum.Right);
            snake.DrawLine();

            ScoreHelper.GetScore(score);

            while (true)
            {
                if (line.Collision(snake) || snake.CollisionWithOwnTail())
                {
                    break;
                }
                if (snake.Eat(food))
                {
                    score++;
                    ScoreHelper.GetScore(score);

                    food = FoodFactory.GetRandomFood(49, 20, 'Х');
                    Console.ForegroundColor = ColorHelper.GetRandomColor(new Random().Next(1, 5));
                    food.DrawPoint();
                    Console.ResetColor();

                }

                Thread.Sleep(100);
                snake.Move();

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo key = Console.ReadKey();
                    snake.PressKey(key.Key);
                }
            }

            user.Score = score;
            _userService.SaveScore(user);
        }
    }
}
