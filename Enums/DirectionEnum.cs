﻿namespace Snake.Enums

{
    public enum DirectionEnum
    {
        Right,
        Left,
        Up,
        Down
    }
}
