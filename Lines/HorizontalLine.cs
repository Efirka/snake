﻿using System.Collections.Generic;

namespace Snake.Lines
{
    public class HorizontalLine : Shape
    {

        public HorizontalLine(int x, int y, char symbol, int count)
        {
            _points = new List<Point>();
            for (int i = x; i < count; i++)
            {
                Point point = new Point(i, y, symbol);
                _points.Add(point);
            }
        }


    }
}
