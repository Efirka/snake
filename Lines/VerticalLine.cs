﻿using System.Collections.Generic;

namespace Snake.Lines
{
    public class VerticallLine : Shape
    {

        public VerticallLine(int x, int y, char symbol, int count)
        {
            _points = new List<Point>();
            for (int i = y; i < count; i++)
            {
                Point point = new Point(x, i, symbol);
                _points.Add(point);
            }
        }

    }
}
