﻿using System;

namespace Snake.Helpers
{
    public static class ColorHelper
    {
        public static ConsoleColor GetRandomColor(int value)
        {
            switch (value)
            {
                case 1:
                    return ConsoleColor.Red;
                case 2:
                    return ConsoleColor.Blue;
                case 3:
                    return ConsoleColor.Green;
                case 4:
                    return ConsoleColor.Yellow;
                case 5:
                    return ConsoleColor.Cyan;
                default:
                    return ConsoleColor.White;
            }
        }
    }
}
