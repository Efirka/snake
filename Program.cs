﻿using Snake.UI;
using System;

namespace Snake
{
    class Program
    {

        static void Main(string[] args)
        {
            UIService uiService = new UIService();
            uiService.GetMenu();
            while (true)
            {
                ConsoleKeyInfo key = Console.ReadKey();
                uiService.GetCommand(key.Key);
            }


        }
    }
}
