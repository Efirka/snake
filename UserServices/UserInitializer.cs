﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake.UserServices
{
    public static class UserInitializer
    {
        public static List<User> GetSampleUserData ()
        {
            List<User> users = new List<User> ();

            users.Add(new User() { Name = "Vlad", Score = 126});
            users.Add(new User() { Name = "Ihor", Score = 123 });
            users.Add(new User() { Name = "Player", Score = 204 });

            return users;
        }
    }
}
