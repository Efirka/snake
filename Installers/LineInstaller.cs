﻿using Snake.Lines;
using System;
using System.Collections.Generic;

namespace Snake.Installers
{
    public class LineInstaller
    {
        private List<Shape> _shapes;

        public LineInstaller()
        {
            _shapes = new List<Shape>();
            HorizontalLine upLine = new HorizontalLine(0, 0, '-', 50);
            HorizontalLine downLine = new HorizontalLine(0, 20, '-', 50);

            VerticallLine leftLine = new VerticallLine(0, 1, '|', 20);
            VerticallLine rightLine = new VerticallLine(49, 1, '|', 20);

            _shapes.AddRange(new List<Shape> { upLine, downLine, leftLine, rightLine });



        }

        public void DrawShapes()
        {
            foreach (var item in _shapes)
            {
                item.DrawLine();
            }
        }

        internal bool Collision(Shape shape)
        {
            foreach (var item in _shapes)
            {
                if(item.Collision(shape))
                   return true;
                
            }
            return false;
        }
    }
}
