﻿using Snake.Helpers;
using Snake.UserServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake.UI
{
    public class UIService
    {
        private GamePlay _gameplay = new GamePlay();
        private UserService _userService = new UserService();   
        private User _user = new User();   
        public void GetMenu()
        {
            Console.Clear();
            Console.SetCursorPosition(10, 5);
            Console.WriteLine("||===========================================||");
            Console.SetCursorPosition(10, 6);
            Console.WriteLine("||-------------------------------------------||");
            Console.SetCursorPosition(10, 7);
            Console.WriteLine("||         Welcome to the Snake Game         ||");
            Console.SetCursorPosition(10, 8);
            Console.WriteLine("||-------------------------------------------||");
            Console.SetCursorPosition(10, 9);
            Console.WriteLine("||===========================================||");
            Console.SetCursorPosition(10, 10);
            Console.WriteLine("||        -Press Enter to start game-        ||");
            Console.SetCursorPosition(10, 11);
            Console.WriteLine("||                                           ||");
            Console.SetCursorPosition(10, 12);
            Console.WriteLine("||      -Use arrows to move the snake-       ||");
            Console.SetCursorPosition(10, 13);
            Console.WriteLine("||       -Press C to create the user-        ||");
            Console.SetCursorPosition(10, 14);
            Console.WriteLine("||        -Press S to get all scores-        ||");
            Console.SetCursorPosition(10, 15);
            Console.WriteLine("||       -Press ESC to quite the game-       ||");
            Console.SetCursorPosition(10, 16);
            Console.WriteLine("||-------------------------------------------||");
            Console.SetCursorPosition(10, 17);
            Console.WriteLine("||===========================================||");

        }

        public void GetCommand(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.Enter:
                    StartGame();
                    break;
                case ConsoleKey.C:
                    CreateUserForm();
                    break;
                case ConsoleKey.S:
                    ScoreBoard();
                    break;
                case ConsoleKey.Escape:
                    Environment.Exit(0);
                    break;
                default: 
                    GetMenu();
                    break;
            }
        }

        private void StartGame()
        {
            Console.Clear();
            _gameplay.StartGame(_user);
            Consede();
        }

        private void Consede()
        {
            
            Console.Clear();

            Console.WriteLine("Game Over");
            Console.WriteLine("To try game press Enter");
            Console.WriteLine("Back to menu press Backspace");
        }

        private void CreateUserForm()
        {
            Console.Clear();

            CreateUser:
            Console.Write("Enter your name: ");
            string userName = Console.ReadLine();

            try
            {
                _user = _userService.CreateUser(userName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                goto CreateUser;
            }

            Console.WriteLine("User was saved");
            Console.WriteLine("\nPress Backspace to back");

        }

        private void ScoreBoard()
        {
            Console.Clear();

            var users = _userService.GetAllUsers();

            foreach (var user in users)
            {
                Console.WriteLine($"{user.Name} with score {user.Score}");
            }
            Console.WriteLine("\nPress Backspace to back");
        }



 
    }
}
